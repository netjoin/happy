<?php
if (!class_exists('Boot', false)) {
	Yaf\Loader::getInstance()->registerLocalNameSpace('Boot');
}

class Bootstrap extends Boot
{
	public function _init()
	{
		parent::_init();
	}
}
