<?php

class UserPlugin extends Yaf\Plugin_Abstract
{
	public function routerStartup(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
		var_dump("routerStartup");
	}
	
	public function routerShutdown(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
		var_dump("routerShutdown");
		
		// 在路由结束以后, 获取起作用的路由协议
		var_dump(Yaf\Dispatcher::getInstance()->getRouter()->getCurrentRoute());
	}
	
	public function dispatchLoopStartup(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
		var_dump("dispatchLoopStartup");
	}
	
	public function dispatchLoopShutdown(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
		var_dump("dispatchLoopShutdown");
	}
	
	public function preDispatch(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
		var_dump("preDispatch");
	}
	
	public function postDispatch(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
		var_dump("postDispatch");
	}
	
	public function preResponse(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
		var_dump("postResponse");
	}
}
