<?php
/**
 * Yaf 自定义路由协议 Demo
 * 自定义继承 Yaf\Request_Abstract，为的是修改protected params参数
 *
 * @author   Benny Wu <contact@wubenli.com>
 * @version  0.1
 */

class Route_Custom  extends Yaf\Request_Abstract implements Yaf\Route_Interface
{

    protected $_route = null;
	 
	public function __construct($uri = null, $trim = '/', $split = '-')
	{
		if (! $uri) {
			$uri = $_SERVER['REQUEST_URI'];
		}
		$this->_route = explode($split, trim($uri, $trim));
	}
	
	
	/**
     * Route 实现，继承实现Yaf_Router_Interface route 
     *
     * @access public 
     * @param  Object(Yaf_Request_Http) $req 默认参数
     * @return boole  true
     */
	 
    public function route($req)
	{
        $uri = $this->_route;
        // Test 测试匹配路由为 admin，将模块设定admin
        // URL  路径就成了 /admin-:controller-:action-:params
        if($uri[0] == 'module'){
            $req->module = 'index';
            $req->controller = !empty($uri[1])?$uri[1]:'';
            $req->action = !empty($uri[2])?$uri[2]:'';
            if(!empty($uri[3])){
                $param = array();
                $params = array_slice($uri, 3);
                foreach ( $params as $key => $value) {
                    if( $key %2 == 0){
                        $param[$params[$key]] = $params[$key+1];
                    }
                }
                $req->params = $param;
            }
			return true;
        }
        return false;
    }

    public function assemble(array $mvc, array $query = null)
	{
        return true;
    }
}
