<?php
/**
 * 原型引导文件
 */

class Boot extends Yaf\Bootstrap_Abstract
{
	public $config = null;
	
	/* 初始化 */
	public function _init()
	{
		#$db = Yaf_Registry::get("db");
        #Yaf\Registry::set("config", $conf);
		$this->config = Yaf\Application::app()->getConfig();
	}
	
	/**
	 * 自动加载
	 */
	public function _initLoader(Yaf\Dispatcher $dispatcher)
	{
		# global $conf;
		$conf = $this->config;
		
		/* 注册本地类前缀 */
		if (@ $conf->loader->namespace) {
			$perfix = explode(';', $conf->loader->namespace);
			Yaf\Loader::getInstance()->registerLocalNameSpace($perfix);
		}
		
		/* 导入PHP文件 */
		if (@ $conf->loader->import) {
			foreach ($conf->loader->import as $file) {
				Yaf\Loader::import($file);
			}
		}
	}
	
	/**
	 * 插件
	 */
	public function _initPlugin(Yaf\Dispatcher $dispatcher)
	{
		$router = $dispatcher->getInstance()->getRouter();
		if ($this->config->routes){
			$router->addConfig($this->config->routes);
		}
		
		/* 后面的规则覆盖前面的 
		
		
		// 覆盖前面的 simple
		$route_simple  = new Yaf\Route\Simple('m1', 'c1', 'a1');
		$router->addRoute("query_string", $route_simple);
		
		$route_supervar = new Yaf\Route\Supervar("r1");
		$router->addRoute("compatible", $route_supervar);
		
		// 使用 REQUEST_URI 作为控制器或动作
		$route_map = new Yaf\Route\Map(false, '!'); //路径分隔符/!/
		$router->addRoute("request_uri", $route_map);
		
		$route = new Yaf\Route\Rewrite(
           "/product/:ident",
           array(
               'controller' => "index",
			   'action' => "dummy",
           )
        );
		$router->addRoute('product', $route);
		
		$route = new Yaf\Route\Regex(
           "#^/product/([^/]+)/([^/])+#", 
           array(
              'controller' => ":name",  //使用上面匹配的:name, 也就是$1作为controller
           ),
           array(
              1 => "name",   // now you can call $request->getParam("name")
              2 => "id",     // to get the first captrue in the match pattern.
           )
        );
		$router->addRoute('reg', $route);
		*/
		
		$route = new Route_Custom(); //'index-index-test-param-value'
		$router->addRoute('custom', $route);
		
		$user = new UserPlugin();
		# $dispatcher->registerPlugin($user);
	}
	
	/**
	 * 数据库
	 */
	public function _initDatabase(Yaf\Dispatcher $dispatcher) 
    { 
		if ($this->config->database) {
			Yaf\Registry::set('db', new Database($this->config->database->toArray()));
		}
    }
	
	/*
	public function _initTwig(\Yaf\Dispatcher $dispatcher)
	{
		$twig = new \Twig\Adapter(APP_PATH . "/application/views/", $this->config->get("twig")->toArray());
		$dispatcher->setView($twig);
	}*/
}
