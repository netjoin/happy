<?php

namespace Database;

class PhpMysqli
{
	public $dbh = null;
	public $host = null;
	public $username = null;
	public $passwd = null;
	public $dbname = null;
	
	public function __construct($config = [])
	{
		if ($config) {
			$this->init($config);
		}
	}
	
	public function init($config = [])
	{
		$this->setVar($config);
		$this->getDbh();
	}
	
	public function setVar($arg = [])
	{
		foreach ($arg as $key => $value) {
			$this->$key = $value;
		}
	}
	
	public function getDbh()
	{
		$this->dbh = new \mysqli($this->host, $this->username, $this->passwd, $this->dbname);
		if ($this->dbh->connect_errno) {
			printf("Connect failed: %s\n", $this->dbh->connect_error);
			exit();
		}
		return $this->dbh;
	}
	
	public function select($sql = null)
	{
		$result = $this->dbh->query($sql);
		# $row = $result->fetch_all(MYSQLI_ASSOC);
		$arr = [];
		while ($obj = $result->fetch_object()) {
			$arr[] = $obj;
		}
		$result->close();
		return $arr;
	}
	
	public function __destruct()
	{
		try {
			$this->dbh->close();			
		} catch (mysqli_sql_exception $e) {
			echo $e->__toString();
		}
	}
}
