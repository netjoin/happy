<?php

class Database
{
	public static $_adapter = null;
	public $primary_key = null;
	public $join = '';
	public $group_by = '';
	public $having = '';
	
	public function __construct($config = [])
	{
		if (!$config) {
			$config = Yaf\Application::app()->getConfig();
			$config = $config->database->toArray();
		}
		$this->init($config);
	}
	
	public function setVar($arg = [])
	{
		foreach ($arg as $key => $value) {
			$this->$key = $value;
		}
	}
	
	public function init($config = [])
	{
		$this->setVar($config);
		$this->getAdapter($config);
	}
	
	public function getAdapter($config)
	{
		$name = $config['adapter'];
		$name = str_replace('_', ' ', $name);
		$name = ucwords($name);
		$name = str_replace(' ', '', $name);
		$class = '\Database\Php' . $name;
		return self::$_adapter = new $class($config);
	}
	
	public function query($sql = null)
	{
		return self::$_adapter->query($sql);
	}
	
	public function from($name = null)
	{
		return $name = $name ? : $this->dbname . '.' . $this->table_name;
	}
	
	public function sqlSet($data)
	{
		if (!is_array($data)) {
			return $data;
		}
		
		$arr = [];
		foreach ($data as $key => $value) {
			$value = addslashes($value);
			$arr[] = "`$key` = '$value'";
		}
		return $str = implode(", ", $arr);
	}
	
	public function sqlWhere($where, $type = 'AND')
	{
		if (!is_array($where)) {
			return $where;
		}
		
		$arr = [];
		foreach ($where as $key => $value) {
			// 没有列名的直接写SQL语句
			if (is_numeric($key)) {
				$arr[] = $value;
				
			// 多条件
			} elseif (preg_match('/^(ADN|OR)$/', $key, $matches)) {
				print_r([$matches, __FILE__, __LINE__]);
				exit;
			} elseif (is_array($value)) {
				print_r([$value, __FILE__, __LINE__]);
				exit;
			} else {
				$value = addslashes($value);
				$arr[] = "`$key` = '$value'";
			}
		}
		return $str = implode(" $type ", $arr);
	}
	
	public function select($where = null, $column = '*', $order = null, $limit = 10)
	{
		$db_table = $this->from();
		$sql = "SELECT $column FROM $db_table";
		if ($this->join) {
			$sql .= " $this->join";
		}
		
		$where = $this->sqlWhere($where);
		if ($where) {
			$sql .= " WHERE $where";
		}
		
		if ($this->group_by) {
			$sql .= " GROUP BY $this->group_by";
			if ($this->having) {
				$sql .= " HAVING $this->having";
			}
		}
		
		if ($order) {
			$sql .= " ORDER BY $order";
		}
		$sql .= " LIMIT $limit";
		return self::$_adapter->select($sql);
	}
	
	public function __call($name, $arguments)
	{
		return self::$_adapter->$name($arguments[0]);
	}
	
	public function __destruct()
	{
	}
}