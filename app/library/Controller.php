<?php
/**
 * 原型控制器
 */

class Controller extends Yaf\Controller_Abstract
{
	/* 初始化 */
	public function init()
	{
		/* 缺省动作设置 */
		$this->methods = get_class_methods($this);
		$this->action = $this->getRequest()->getActionName();
		$this->params = $this->getRequest()->getParams();
		// 静态化
		if ( preg_match('/(.*)\.(html|htm|php)$/i', $this->action, $matches) ) {
			$this->action = $matches[1];
			$this->_request->setActionName($this->action);
		}
		// 方法检测
		if (! in_array($this->action . 'Action', $this->methods)) {
			// 扫描 action 文件
			$dir = scandir(APP_PATH . '/app/actions');
			foreach ($dir as $file) {
				if (preg_match('/(.*)\.php$/i', $file, $matches)) {
					$action = strtolower($matches[1]);
					$this->methods[] = $action . 'Action';
				}
			}
			
			if (! in_array($this->action . 'Action', $this->methods)) {
				print_r($this->action);
				print_r($this->params);
				# Yaf\Dispatcher::throwException(true);
				# $this->_request->setActionName('_');
			}
		}
	}
	
	/**
	 * 缺省动作
	 */
	public function _Action()
	{
		// 禁用视图
		Yaf\Dispatcher::getInstance()->disableView();
		
		// 异常
		$exception = new Yaf\Exception;
		$e = print_r($exception, true);
		echo <<<HEREDOC
			<pre>$e</pre>
HEREDOC;
	}
	
	/**
	 * 获取变量
	 */
	public function get($key, $value = null, $filter = null, $allow = null)
	{
		$val = $this->_request->getQuery($key, $value);
		return $val;
	}
	
	/**
	 * 动作
	 */
	public function justice_leagueAction()
	{
	}
}
