<?php
/**
 * 默认控制器
 */

use Astrology\Kernel;

class PlayController extends Controller
{
	/* 指定动作分离类 
	public $actions = [
        'dummy2' => "actions/Dummy.php",
    ];*/
			  
	/* 初始化 */
	public function init()
	{
		// 这是必须的
		parent::init();
	}
	
	/* 默认动作 */
	public function indexAction()
	{
		// 视图赋值
		$this->_view->test = __METHOD__;
		
		# print_r(new Kernel);
		# $this->forward('dummy');
	}
	
	
	public function testAction()
	{
		// route_rewrite 参数获取不全
		print_r($this->_request->getParams());
		exit;
	}
	
}
