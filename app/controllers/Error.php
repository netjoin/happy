<?php
/**
 * 默认错误控制器
 */
 
class ErrorController extends Yaf\Controller_Abstract
{
	/* 默认错误动作 */
	public function errorAction(Exception $exception)
	{
		
		Yaf\Dispatcher::getInstance()->autoRender(false);
		#echo json_encode(get_object_vars($this));
		
		#Yaf\Dispatcher::getInstance()->enableView();
		#$this->_view->exception = print_r(new Yaf\Exception, true);
		
		try {
			throw $exception;
		} catch (Yaf\Exception\LoadFailed $e) {
			//加载失败
			print_r([__LINE__, $e->getCode(), $e->getMessage()]);
		} catch (Yaf\Exception $e) {
			//其他错误
			print_r([__LINE__, $e->getCode(), $e->getMessage()]);
		}
	}
}
