<?php
/**
 * API 控制器
 */

class ApiController extends Controller
{
	/* 初始化 */
	public function init()
	{
		// 这是必须的
		parent::init();
		Yaf\Dispatcher::getInstance()->autoRender(false);
	}
	
	/* 默认动作 */
	public function indexAction()
	{
		$page = $this->get('page', 1);
		$limit = $this->get('limit', 10);
		$offset = $limit * $page - $limit;
		$entry = new VideoEntryModel;
		$all = $entry->select(null, 'entry_id,name,poster', 'modified DESC', "$offset,$limit");
		print_r(json_encode($all));
	}
	
	
	public function testAction()
	{
		// route_rewrite 参数获取不全
		print_r($this->_request->getParams());
		exit;
	}
	
}
