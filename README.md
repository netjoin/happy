# Happy
使用 Yaf 开发的完善 PHP 框架

## 开始使用 Yaf

* ### 下载 PHP 扩展
[php_yaf.dll](http://pecl.php.net/package/yaf)

* ### php.ini 配置选项
```
extension=php_yaf.dll

[yaf]
;; 开启命名空间
yaf.use_namespace = 1
```


* ### 配置文件
/app/application.ini
```
;* 应用的绝对目录路径（必须要有）
application.directory = APP_PATH "/app"

;@ 本地类库目录和命名空间前缀
;application.library =  APP_PATH "/lib"
; Yaf 2.1.6 以后也可以
;application.library.directory =  APP_PATH "/lib"
application.library.namespace = "Boot;Test"

;$ yaf runtime 配置 php.ini
application.system.action_prefer = 1
; 全局类库的目录路径
application.system.library = K:\dev\git\Happy\lib
```


* ### 入口文件
/web/index.php
```
define('APP_PATH', dirname(dirname(__FILE__)));

$app = new Yaf\Application(APP_PATH . "/app/application.ini");
$app->bootstrap()->run();
```


* ### 控制器
/app/controllers/Index.php
```
class IndexController extends Yaf\Controller_Abstract
{
    public function indexAction()
    {
		// 是否自动渲染
		Yaf\Dispatcher::getInstance()->autoRender(false);
		
		// 视图赋值
		$this->_view->test = 'Hello World!';
    }
}
```


* ### 模板
/app/views/index/index.phtml
```
<?=$test?>
```


* ### 引导程序
/app/Bootstrap.php
```
class Bootstrap extends Yaf\Bootstrap_Abstract
{
	public function _init()
	{
	}
}
```


* ### 本地类库
/app/library/Controller.php


* ### .user.ini 配置选项
/web/.user.ini
```
[yaf]
; 默认控制器动作优先
yaf.action_prefer = On
```


* ### 服务器重写规则

**Apache**
```
#.htaccess
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule .* index.php
```

**Nginx**
```
server {
  listen ****;
  server_name  domain.com;
  root   document_root;
  index  index.php index.html index.htm;
 
  if (!-e $request_filename) {
    rewrite ^/(.*)  /index.php/$1 last;
  }
}
```

**Lighttpd**
```
$HTTP["host"] =~ "(www.)?domain.com$" {
  url.rewrite = (
     "^/(.+)/?$"  => "/index.php/$1",
  )
}
```


* ### 命令行中使用
$ php request.php  "request_uir=/index/hello"
```
$app = new Yaf_Application("conf.ini");
$app->getDispatcher()->dispatch(new Yaf_Request_Simple());
```


* ### 自动加载
1. 目录映射规则
> **Controller**	默认模块下为{项目路径}/controllers/, 否则为{项目路径}/modules/{模块名}/controllers
>
> **Model**	{项目路径}/models/
>
> **Plugin**	{项目路径}/plugins/
	
2. 导入 PHP 文件

`Yaf\Loader::import($file);`

3. 类的加载规则

类名中必须包含路径信息, 也就是以下划线"_"分割的目录

`Foo_Dummy_Bar`
/app/library/Foo/Dummy/Bar.php


* ### 动作分离类
/app/actions/Dummy.php

1. 两个选项同时开启
```
; 默认控制器动作优先
application.system.action_prefer = 1

; actions 额外的查找逻辑
application.system.st_compatible = 1
```

2. 或者使用固定的，将不会查找 actions 目录
```
class IndexController extends Controller
{
	// 指定动作分离类
	public $actions = [
		'dummy' => "actions/Dummy.php",
	];
}
```


## 工具与参考

* ### 代码生成工具
[yaf_code generator](https://github.com/laruence/yaf/tree/master/tools/cg)
> php-yaf-src/tools/cg/yaf_cg sample


* ### 类签名生成器
[Yaf Classes signature generator](https://github.com/laruence/yaf/blob/master/tools/yaf.php)

[yaf_classes.php](https://github.com/laruence/yaf/blob/master/tools/yaf_classes.php)


* ### 获取配置
```
$this->config = Yaf\Application::app()->getConfig();
```


* ###  对象注册表/仓库
```
Yaf\Registry::set('db', new Database($this->config->database->toArray()));
$db = Yaf_Registry::get("db");
```


* ### 使用插件
/app/plugins/User.php
1. 定义插件
```
class UserPlugin extends Yaf\Plugin_Abstract
{
	function routerStartup(Yaf\Request_Abstract $request, Yaf\Response_Abstract $response)
	{
	}
	function routerShutdown() {
	}
	function dispatchLoopStartup() {
	}
	function dispatchLoopShutdown() {
	}
	function preDispatch() {
	}
	function postDispatch() {
	}
	function preResponse() {
	}
}
```

2. 注册插件
```
class Bootstrap extends Yaf_Bootstrap_Abstract
{
	public function _initPlugin(Yaf\Dispatcher $dispatcher)
	{
        $dispatcher->registerPlugin(new UserPlugin());
    }
}
```


* ### 动作转接
```
class IndexController extends Yaf\Controller_Abstract
{
    public function indexAction()
    {
		$this->forward("login");
	}
	
	public function loginAction()
    {
	}
}
```


* ### 使用路由
1. 添加路由配置
```
; 查询字符串
routes.simple.type = "simple"
routes.simple.controller = c
routes.simple.module = m
routes.simple.action = a

; 超级变量
routes.supervar.type = "supervar"
routes.supervar.varname = r

; 正则
routes.regex.type = "regex"
routes.regex.match = "#^/list/([^/]*)/([^/]*)#"
routes.regex.route.controller = Index
routes.regex.route.action = dummy
routes.regex.map.1 = name
routes.regex.map.2 = value
```
顺序很重要
```
class Bootstrap extends Yaf_Bootstrap_Abstract
{
	public function _initRoute(Yaf\Dispatcher $dispatcher)
	{
		$router = $dispatcher->getInstance()->getRouter();
		$router->addConfig($this->config->routes);
	}
}
```

2. 路由协议
```
Yaf_Route_Simple($module_name, $controller_name, $action_name)
Yaf_Route_Supervar($supervar_name)
Yaf_Route_Static()
Yaf_Route_Map($controller_prefer, $delimiter)
Yaf_Route_Rewrite($match, $route, $verify)
Yaf_Route_Regex($match, $route, $map, $verify)
```
```
$route_supervar = new Yaf\Route\Supervar("r");
$router->addRoute("compatible", $route_supervar);
```


* ### 异常和错误
```
class ErrorController extends Yaf\Controller_Abstract
{
	public function errorAction(Exception $exception)
	{
		try {
			throw $exception;
		} catch (Yaf\Exception\LoadFailed $e) {
			//加载失败
		} catch (Yaf\Exception $e) {
			//其他错误
		}
		$this->_view->code = $exception->getCode()
		$this->_view->message = $exception->getMessage()
		$this->_view->exception = print_r(new Yaf\Exception, true);
	}
}
```


* ### 使用模块
/app/modules/Admin/controllers/Index.php
```
;! 模块
application.modules = index,admin
```