<?php
/**
 * 入口文件
_*/

/* 预定义 */
define('APP_PATH', dirname(dirname(__FILE__)));
$environ = ''; //脚本缺省环境
// 错误报告
if ('develop' == $environ || isset($_GET['debug'])) {
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}
// 调试信息
if (isset($_GET['phpinfo']) || '/phpinfo' == @$_SERVER['PATH_INFO']) {
	phpinfo();
	exit;
}


/* 运行模式 */
$request_uri = null;
switch (PHP_SAPI) {
	case 'cgi-fcgi':
		# echo PHP_VERSION;
		break;
	case 'cli':
		if (1 < $argc) {
			$request_uri = $argv[1];
			if (preg_match('/^request_uri\=\/(.*)/i', $request_uri, $matches)) {
				$request_uri = '/' . $matches[1];
				
			} elseif (! preg_match('/^\//', $request_uri)) {
				$request_uri = false;
				parse_str(implode('&', array_slice($argv, 1)), $_GET);
			}
			
			if (false !== $request_uri && 2 < $argc) {
				parse_str(implode('&', array_slice($argv, 2)), $_GET);
			}
			$_SERVER['REQUEST_URI'] = $request_uri;
		}
		break;
	default:
		echo PHP_SAPI;
		break;
}


/* 配置文件 */
$config = APP_PATH . "/app/yaf.ini";
# $config = ['application' => ['directory' => APP_PATH . "/app"]];
if ('string' == gettype($config)) {
	$ini = new Yaf\Config\Ini($config);
	$environ = @$ini->yaf->application->system->environ ? : $environ; //配置自定义环境
	if ($environ) {
		$config = $ini->$environ ? $ini->$environ->toArray() : $ini->yaf->toArray();
	} else {
		$config = $ini->yaf->toArray();
	}
}


/* 启动 Yaf */
define('APP_ENVIRON', $environ ? : ini_get('yaf.environ')); //系统最终环境
$app = new Yaf\Application($config, APP_ENVIRON); //section 只有是文件路径时有效
if ('cli' != PHP_SAPI) {
	$app->bootstrap()->run();
} else {
	// 命令行下请求自定义
	$conf = Yaf\Application::app()->getConfig();
	if (@ $conf->request->simple) {
		$simple = $conf->request->simple;
		$request = new Yaf\Request\Simple(
			$simple->method, 
			$simple->module, 
			$simple->controller, 
			$simple->action, 
			$simple->params->toArray()
		);
		
	} elseif ($request_uri) {
		$request = new Yaf\Request\Http($request_uri);
	} else {
		$request = new Yaf\Request\Simple();
	}
	$app->bootstrap();
	$app->getDispatcher()->dispatch($request);
}


/* 调试问题 */
if (@ $debug = $_GET['debug']) {
	eval($debug);
	# print_r(ini_get_all('yaf'));
}
