require.config({
	//baseUrl : 'http://taurus/template/yingmi/js/',
    paths : {
        'jquery': ['http://code.jquery.com/jquery-3.3.1.min', 'jquery']
    },
	shim: {
        underscore : {
            exports : "_"
        },
		"var" : ["app"],
        'jquery.form' : {
            deps : ["jquery3"]
        },
		util: ['css!../css/ui.css']
    },
	map : {
		'*': {
			'css': 'css'
		}
	}
});
