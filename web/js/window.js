window.XHR = [];

var API = {};
API[ 0 ] = function () {
	_( 'ol' )[ 0 ].innerHTML = '';
	var json = JSON.parse( XHR[ 0 ].responseText ),
		len = json.length;
	for ( var i = 0; i < len; i++ ) {
		var row = json[ i ];
		var html = '<li><a href="/video/' + row.entry_id + '"><img src="' + row.poster + '"><p>' + row.name + '</p></a></li>';
		_( 'ol' )[ 0 ].insertAdjacentHTML( 'beforeEnd', html );
	}
}

window.API = API;
