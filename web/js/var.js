( function( global, factory ) {

	"use strict";

	if ( typeof module === "object" && typeof module.exports === "object" ) {

		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
	var XHR = window.XHR || [],
		API = window.API || [];
		
	var VarJS = function ( selector, index, doc ) {
		var result = null,
			key = -1;
		if ( 'number' == typeof index ) {
			key = index;
		}
		if ( 'string' == typeof selector ) {
			doc = doc || document;
			result = doc.querySelectorAll( selector );
			if ( -1 < key ) {
				result = result[ key ];
			}
		}
		return result;
	}
	
	VarJS.api = function () {
		var uri = 0,
			method = 'get',
			url = '/api';
		XHR[ uri ] = new XMLHttpRequest();
		XHR[ uri ].onreadystatechange = function()
		{
			if ( 4 == XHR[ uri ].readyState ) {
				if ( 200 == XHR[ uri ].status ) {
					//alert( XHR[ uri ].responseText );
					API[ uri ]();
				} else {
					alert( 'Problem retrieving data: ' + XHR[ uri ].statusText );
				}
			}
		}
		XHR[ uri ].open( method, url, true );
		XHR[ uri ].send();
	}
	
	if ( typeof define === "function" && define.amd ) {
		define( "var", [], function() {
			return VarJS;
		} );
	}
	
	if ( !noGlobal ) {
		window._ = VarJS;
	}

    return VarJS;
} );
